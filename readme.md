# Notice
This repo stores both WARP Kafka and Portainer Provisioning for Datastream.

# WARP

The CI/CD automatically provisions Kafka Cluster builds in the production server.

NOTE: Running the CI/CD -> Stopping curent instance and build a new one.

## Current Kafka Infrastructure
Currently, this is the mapping between service <> exposed port provisioned in this repo is

- 1 Zookeeper Instance
- 3 Kafka Brokers
- Kafka Manager
- Kafka Schema Registry
- Kafka Schema Registry UI

Detailed status about each deployment can be accessed through *Portainer*.

### Server Side Set-Up
- Please add private SSH key in *variables* section of CI/CD settings
- Create `.env` file from `example.env` file
- Create `warp_deployment.sh` file in the CI/CD target folder. The example script is below.

```
echo "WARP PROVISIONING IS STARTING....."

# Move to working directory
cd /home/admin/docker_utils/warp

# Pull latest version
git checkout master
git pull 'git@gitlab.cs.ui.ac.id:csui-datastream/warp.git' master

# Stack Provisioning
bash run_kafka_env.sh

echo "WARP PROVISIONING DONE....."
```
Author: Favian Kharisma Hazman

# stacks
Provisioning scripts for Datastream layers with Docker-compose. These containers are equipped with autorestart.

## Setting Datastream Up
Prerequisites: Docker is installed, and Portainer is running using `./run_portainer.sh`
1. Make sure `warp` is running (https://gitlab.cs.ui.ac.id/csui-datastream/warp)
2. Setup Debezium docker in `debeziun-infra/`
3. Setup Elasticsearch docker in `elastic/`
4. Setup Influx docker in `influx/`
5. Setup Flink Cluster in `flink-cluster/`
6. Monitoring stacks and Grafana comes last. Setup the cluster in `monitoring/`

## Notes
Please add to help other developers
1. [FAVIAN] Flink cluster task managers can be scaled with 
`docker-compose scale taskmanager=<NUM>`
2. [FAVIAN] For Elasticsearch cluster set this in host server:
```
grep vm.max_map_count /etc/sysctl.conf
vm.max_map_count=262144
```
